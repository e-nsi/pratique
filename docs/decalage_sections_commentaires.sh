#!/bin/sh

fichiers="N?/*/*/exo_REM.md"
a_faire=$(grep -l "^# Commentaires" $fichiers)
#echo $a_faire
#niveau_3=$(grep -l "^###[^#]" $fichiers)
for fich in $a_faire
do
	sed -i 's/^##* [^#]*$/#&/' $fich
	git add $fich
	echo "Correction de $fich"
done
