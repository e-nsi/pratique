# Tests
assert fib_2(0) == (1, 0)
assert fib_2(1) == (0, 1)
assert fib_2(2) == (1, 1)
assert fib_2(3) == (1, 2)
assert fib_2(9) == (21, 34)
assert fib_2(4) == (2, 3)


# Autres tests

fib_memo = [0, 1]
for _ in range(100):
    fib_memo.append(fib_memo[-1] + fib_memo[-2])

for n in range(1, 100):
    f_n, f_nm1 = (fib_memo[n-1], fib_memo[n])
    assert fib_2(n) == (f_n, f_nm1), f"Erreur avec {n=}"


def secret799_fib_2(n):
    """Renvoie (F_{n - 1}, F_{n})
        deux nombres de Fibonacci consécutifs
    on utilise les variables f_nm1 et f_n
    """
    # cas de base
    if n == 0:
        f_nm1 = 1
        f_n = 0
    elif n == 1:
        f_nm1 = 0
        f_n = 1
    
    # cas général
    elif n % 2 == 0:
        # n est pair
        k = n // 2
        f_km1, f_k = secret799_fib_2(k)
        f_nm1 = f_k * f_k  +  f_km1 * f_km1
        f_n = f_k * (f_k  +  2 * f_km1)
    else:
        # n est impair
        k = n - 1
        f_km1, f_k = fib_2(k)
        f_nm1 = f_k
        f_n = f_km1 + f_k

    return f_nm1, f_n

# gros tests
for e in range(2, 7):
    n = 10**e
    attendu = secret799_fib_2(n)
    assert fib_2(n) == attendu, f"Erreur avec {n=}"
