def fib_2(n):
    ...

# Tests
assert fib_2(0) == (1, 0)
assert fib_2(1) == (0, 1)
assert fib_2(2) == (1, 1)
assert fib_2(3) == (1, 2)
assert fib_2(9) == (21, 34)
assert fib_2(4) == (2, 3)

