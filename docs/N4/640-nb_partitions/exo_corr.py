def nb_sommes(n):
    def nb_k_sommes(n, k):
        if n < k:
            return 0
        elif k == 1:
            return 1
        else:
            return nb_k_sommes(n - 1, k - 1) + nb_k_sommes(n - k, k)
    
    return sum(nb_k_sommes(n, k) for k in range(1, 1 + n))


# tests

assert nb_sommes(3) == 3
assert nb_sommes(5) == 7
