# Pour aller plus loin

Il existe un algorithme pour calculer $S_n$ sans avoir besoin de calculer tous les précédents. C'est un exercice très difficile, où on utilise de manière surprenante le triangle de Pascal. Par exemple, 

$$S_{10^{18}} = 257\,124\,935\,176\,141\,732\,152\,547\,069\,538\,475\,658$$

La méthode ci-dessus aurait besoin de la mémoire d'environ un milliard d'ordinateurs, et d'un temps de calcul de plusieurs dizaines d'années. Avec un bon algorithme, c'est quasi instantané.
