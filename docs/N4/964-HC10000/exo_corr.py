a = [None, 1, 1]
S = [None, 1, 2]

cumul = 2
for n in range(3, 10000):
    a_n = a[a[n-1]] + a[n - a[n-1]]
    a.append(a_n)
    cumul += a_n
    S.append(cumul)



# tests

assert S[1] == 1
assert S[2] == 2
assert S[3] == 4
assert S[4] == 6
assert len(S) >= 10000
