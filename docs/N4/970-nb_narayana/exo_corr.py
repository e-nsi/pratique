factorial = [1]

def narayana(n, k):
    i = len(factorial)
    while n >= i:
        f_i = factorial[i - 1] * i
        factorial.append(f_i)
        i += 1
    return (
          (factorial[n] // factorial[k] // factorial[n - k])
        * (factorial[n] // factorial[k - 1] // factorial[n - k + 1])
        // n
    )



# tests

assert narayana(4, 3) == 6
assert narayana(5, 3) == 20
