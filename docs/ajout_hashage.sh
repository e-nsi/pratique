#!/bin/sh

nom_fichier_secret="clef.txt"


ajout_hash () {
  dossier=$1
  fichier_clef="$dossier/$nom_fichier_secret"
  if [ -d "$dossier" ]
  then
      fichier_clef="$dossier/$nom_fichier_secret"
      #echo "le fichier secret est $fichier_clef"      
      if [ ! -f "$fichier_clef" ]
      then
	      echo $RANDOM$RANDOM > "$fichier_clef"
	      git add "$fichier_clef"
      fi
      hash=$(echo -e "e-nsi+$(cat ${fichier_clef})" | md5sum | sed s/\ .*//)
      if [ ! -d "$dossier/$hash" ]
      then
	      mkdir "$dossier/$hash"
      fi
      for fichier_corr in "$dossier/"*_corr.py
      do
	      if [ -f "$fichier_corr" ]
	      then
		      git mv "$fichier_corr" "$dossier/$hash/"
	      fi
      done
      for fichier_rem in "$dossier/"*_REM.md
      do
	      if [ -f "$fichier_rem" ]
	      then
		      git mv "$fichier_rem" "$dossier/$hash/"
	      fi
      done
      #mv "$dossier/"*_corr.py "$dossier/"*_REM.md "$dossier/$hash/" 2>&-
  fi
}


if [ $# = 0 ]
then
    echo "Déplace les fichiers de correction et de remarques dans un dossier secret :"
    echo "Usage : verif_niveau.sh DOSSIER [DOSSIER]*"
    echo "DOSSIER est un dossier d'exercice (N1/mon_exo)"
    exit 1
else
    while [ $# -gt 0 ]
    do
        ajout_hash "$1"
	shift
    done
fi


