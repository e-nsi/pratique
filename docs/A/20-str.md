# Manipulation de chaines de caractères

- [Dentiste](../N1/210-dentiste/sujet.md) : supprimer les voyelles d'une chaine de caractères
- [Mots qui se correspondent](../N1/210-correspond_mot/sujet.md) : comparer deux chaines de caractères
- [Renverser une chaine](../N1/226-renverse_chaine/sujet.md) : comme son nom l'indique !
- [Collage](../N1/228-join/sujet.md) : former une chaine à partir des éléments d'une liste... réécrire `" ".join(mots)` !
- [Découpe](../N1/238-split/sujet.md) : découper une chaine à chaque espace... réécrire `chaine.split(' ')` !
- [Code de César](../N1/421-code_cesar/sujet.md) : chiffrer une chaine de caractère à l'aide du code de César
- [Texte inclus](../N2/150-est_inclus/sujet.md) : recherche d'un motif dans une chaine de caractères
