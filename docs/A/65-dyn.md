# Programmation dynamique

- [Communication des acacias](../N3/900-acacias/sujet.md) : somme maximale dans un tableau sous contrainte
- [Plus longues sous-chaines](../N3/960-diff/sujet.md) : plus longues sous-chaines communes à deux chaines
