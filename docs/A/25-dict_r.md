# Utilisation de dictionnaires

On y parcourt des tables de hachage.

- [Anniversaires](../N1/300-anniversaires/sujet.md) : déterminer les clés dont les valeurs associées vérifient une certaine condition
- [Couleurs](../N1/320-couleurs/sujet.md) : convertir la représentation d'une couleur en hexadécimal à du RGB
- [L-système](../N1/320-lsystem/sujet.md) : « calculer » une nouvelle chaine de caractères en respectant les règles contenues dans un dictionnaire
- [Top-like](../N1/330-top_like/sujet.md) : déterminer la clé de valeur maximale
