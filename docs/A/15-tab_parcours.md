# Parcours de tableaux

Ici l'on filtre des tableaux, on vérifie qu'un tableau est trié.

- [Écrêtage](../N2/100-ecretage/sujet.md) : remplacer les valeurs extrêmes d'un tableau 
- [Soleil couchant](../N1/120-soleil_couchant/sujet.md) : déterminer tous les *maxima* relatifs d'un tableau
- [Nombres puis double](../N1/128-nb_puis_double/sujet.md) : rechercher des couples de valeurs consécutives particulières dans un tableau
- [Est trié ?](../N1/216-est_trie/sujet.md) : le tableau est-il trié ?
- [Liste des différences](../N1/213-liste_differences/sujet.md) : comparer deux tableaux
- [Gelées](../N1/225-gelee/sujet.md) : longueur d'un sous-tableau particulier
- [Tous différents](../N1/420-tous_differents/sujet.md) : les éléments d'un tableau sont-ils tous différents ?
