def channiv(age, num_vie, lang="fr"):
    if lang == "fr":
        ...
    elif lang == "en":
        ...
    else:
        return "Je donne ma langue au chat"


# Tests

assert channiv(3, 4) == 'Joyeux 3e channiversaire de ta 4e vie'
assert channiv(3, 4, "en") == 'Happy purrthday, for 3rd year of 4th life'
assert channiv(1, 1, "fr") == 'Joyeux 1er channiversaire de ta 1re vie'
assert channiv(1, 1, "en") == 'Happy purrthday, for 1st year of 1st life'
assert channiv(3, 4, "miaou") == 'Je donne ma langue au chat'
