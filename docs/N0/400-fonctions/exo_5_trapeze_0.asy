// © 2022, Franck Chambon

size(6cm, 0);

real h = 4, dx = 3, l = 6;
pair H = (0, 0), A = (-dx, 0), B = (l + dx/2, 0);
pair C = (l, h), D = (0, h);

draw(A--B--C--D--cycle, 1bp+black);
//dot("$A$", A, dir(D--A,B--A));
//dot("$B$", B, dir(C--B,A--B));
//dot("$C$", C, dir(B--C,D--C));
//dot("$D$", D, dir(A--D,C--D));

draw(H--D, dashed);
draw(scale(0.2*dx) * unitsquare);

label("$b_1$", (A + B)/2, S);
label("$b_2$", (C + D)/2, N);
label("$h$", (H + D)/2, E);


shipout(bbox(2.5mm, nullpen));

