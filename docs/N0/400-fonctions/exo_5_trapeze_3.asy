// © 2022, Franck Chambon

import math;
unitsize(1cm);

add(grid(5, 4, black+dotted));
pair A = (1, 1), B = (4, 1), C = (4, 3), D = (1, 3);
pair H = A;

draw (A--B--C--D--cycle, 1bp+black);


label("$3$", (A + B)/2, S);
label("$3$", (C + D)/2, N);
label("$2$", (H + D)/2, NE);

label("L'aire est de 6.", (2.5, -0.5));

shipout(bbox(2.5mm, nullpen));

