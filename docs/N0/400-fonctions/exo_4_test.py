
# tests

assert produit(2, 3) == 6
assert produit(-5, 2) == -10


# autres tests

for k in range(-10, 20):
    for n in range(-10, 20):
        attendu = k * n
        assert produit(k, n) == attendu, f"Erreur avec {k=} et {n=}"
