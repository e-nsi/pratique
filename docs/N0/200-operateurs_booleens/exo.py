def non(a):
    ...


def et(a, b):
    ...


def ou(a, b):
    ...


def ou_exclusif(a, b):
    ...


def non_ou(a, b):
    ...


def non_et(a, b):
    ...


# Tests
assert non(True) == False
assert et(True, False) == False
assert ou(True, False) == True
assert ou_exclusif(True, True) == False
assert non_ou(False, True) == False
assert non_et(False, True) == True
