def plage(n, signe):
    ...


# Tests
assert plage(1, False) == (0, 1)
assert plage(1, True) == (-1, 0)
assert plage(8, False) == (0, 255)
assert plage(8, True) == (-128, 127)
