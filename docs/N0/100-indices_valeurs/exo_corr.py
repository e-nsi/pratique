def somme_par_indices_pairs(nombres):
    somme = 0
    for i in range(len(nombres)):
        if i % 2 == 0:
            somme += nombres[i]

    return somme


def somme_des_valeurs_paires(nombres):
    somme = 0
    for x in nombres:
        if x % 2 == 0:
            somme += x

    return somme


# Tests
assert somme_par_indices_pairs([]) == 0
assert somme_des_valeurs_paires([]) == 0
assert somme_par_indices_pairs([4, 6, 3]) == 7
assert somme_des_valeurs_paires([4, 6, 3]) == 10
