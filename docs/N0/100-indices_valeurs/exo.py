def somme_par_indices_pairs(nombres):
    somme = ...
    for ... in ...:
        if ...:
            somme = ...

    return ...

def somme_des_valeurs_paires(nombres):
    somme = ...
    for ... in ...:
        if ...:
            somme = ...

    return ...

# Tests
assert somme_par_indices_pairs([]) == 0
assert somme_des_valeurs_paires([]) == 0
assert somme_par_indices_pairs([4, 6, 3])== 7
assert somme_des_valeurs_paires([4, 6, 3])== 10