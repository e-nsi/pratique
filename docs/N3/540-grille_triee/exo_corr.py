def recherche(cible, grille):
    n = nb_lignes(grille)
    m = nb_colonnes(grille)
    i = 0
    j = m - 1
    while (i < n) and (j >= 0):
        x = donne_valeur(grille, i, j)
        if   x > cible:
            j -= 1
        elif x < cible:
            i += 1
        else:
            return (i, j)
    return None




# tests

# Pour ce test public grille est une liste Python,
#  mais ce ne sera pas toujours le cas !

grille = [
    [11, 33, 42, 63],
    [20, 52, 67, 80],
    [25, 61, 88, 95],
]

def nb_lignes(grille):
    return len(grille)

def nb_colonnes(grille):
    return len(grille[0])

def donne_valeur(grille, i, j):
    global cout
    assert 0 <= i < 3
    assert 0 <= j < 4
    cout += 1
    return grille[i][j]

cout = 0
resultat = recherche(42, grille)
assert cout <= 7, "Trop de tentatives"
assert resultat == (0, 2), "Mauvaises coordonnées"

cout = 0
resultat = recherche(24, grille)
assert cout <= 7, "Trop de tentatives"
assert resultat is None, "La cible est absente"

