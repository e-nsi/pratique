def est_tas_valide(rollers):
    ...


# Tests
assert est_tas_valide([]) == True
assert est_tas_valide([("gauche", 40), ("droit", 40)]) == True
assert est_tas_valide([("gauche", 40), ("droit", 40), ("gauche", 41)]) == False
