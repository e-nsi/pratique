def est_tas_valide(rollers):
    paires = [0 for pointure in range(36, 47)]
    for genre, pointure in rollers:
        if genre == "gauche":
            paires[pointure - 36] += 1
        else:
            paires[pointure - 36] -= 1

    for valeur in paires:
        if valeur != 0:
            return False
    return True


# Tests
assert est_tas_valide([]) == True
assert est_tas_valide([("gauche", 40), ("droit", 40)]) == True
assert est_tas_valide([("gauche", 40), ("droit", 40), ("gauche", 41)]) == False
