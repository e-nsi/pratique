def lplsc(texte_a, texte_b):

    def lplsc_rec(i_a, i_b):
        if i_a == ... or ...:
            return ...
        if (i_a, i_b) ...:
            if texte_a[i_a] == ...:
                memoire[...] = 1 + ...
            else:
                memoire[...] = ...

        return ...

    i_a = ...
    i_b = ...
    memoire = ...
    return lplsc_rec(..., ...)


# Tests
texte_a = "lapin"
texte_b = "caprin"
assert lplsc(texte_a, texte_b) == 4

texte_a = "abcd"
texte_b = "abcde"
assert lplsc(texte_a, texte_b) == 4

texte_a = "aBaBaBaB"
texte_b = "aaa"
assert lplsc(texte_a, texte_b) == 3
