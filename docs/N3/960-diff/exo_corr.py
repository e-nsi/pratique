def lplsc(texte_a, texte_b):

    def lplsc_rec(i_a, i_b):
        if i_a == -1 or i_b == -1:
            return 0
        if (i_a, i_b) not in memoire:
            if texte_a[i_a] == texte_b[i_b]:
                memoire[(i_a, i_b)] = 1 + lplsc_rec(i_a - 1,
                                                    i_b - 1)
            else:
                memoire[(i_a, i_b)] = max(lplsc_rec(i_a - 1, i_b),
                                          lplsc_rec(i_a, i_b - 1))
        return memoire[(i_a, i_b)]

    i_a = len(texte_a) - 1
    i_b = len(texte_b) - 1
    memoire = {}
    return lplsc_rec(i_a, i_b)


# Tests
texte_a = "lapin"
texte_b = "caprin"
assert lplsc(texte_a, texte_b) == 4

texte_a = "abcd"
texte_b = "abcde"
assert lplsc(texte_a, texte_b) == 4

texte_a = "aBaBaBaB"
texte_b = "aaa"
assert lplsc(texte_a, texte_b) == 3
