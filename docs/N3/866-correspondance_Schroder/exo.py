def arbre_vers_chemin(arbre):
    ...


# tests

assert arbre_vers_chemin(
    [[[], []], [], []]
) == [(1, 1), (1, 1), (1, -1), (2, 0), (1, -1)]

assert arbre_vers_chemin(
    [[], [[], []], []]
) == [(1, 1), (2, 0), (1, 1), (1, -1), (1, -1)]

assert arbre_vers_chemin(
    [[], [[], [[], []], [], [], []], []]
) == [(1, 1), (2, 0), (1, 1), (2, 0), (1, 1), (1, -1), (2, 0), (2, 0), (1, -1), (1, -1)]


