# tests

assert produit_maxi([0, 1, 2, 3, 2, 1, 0], 3) == 12
assert produit_maxi([0, 1, 2, 3, 2, 1, 0], 1) == 3


# autres tests

assert produit_maxi([0], 1) == 0
assert produit_maxi([0, 7], 1) == 7
assert produit_maxi([7, 0], 1) == 7
assert produit_maxi([5, 7], 1) == 7
assert produit_maxi([7, 5], 1) == 7
assert produit_maxi([5, 7], 2) == 35
assert produit_maxi([7, 5], 2) == 35
assert produit_maxi([1, 5, 7], 2) == 35
assert produit_maxi([7, 5, 1], 2) == 35
assert produit_maxi([0, 10, 2, 3, 2, 9, 0], 4) == 120
assert produit_maxi([0, 9, 2, 3, 2, 10, 0], 4) == 120
assert produit_maxi([2, 2, 2, 3, 2, 2, 2], 2) == 6
assert produit_maxi([0, 2, 2, 0, 2, 2, 0], 3) == 0
assert produit_maxi([0, 2, 2, 0, 2, 2, 1], 3) == 4




