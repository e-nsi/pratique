---
author: Franck Chambon
title: k-produit maximal
tags:
  - 9-prog.dynamique
  - 4-maths
---
# Produit maximal de k termes consécutifs

Écrire une fonction telle que `produit_maxi(valeurs, k)` renvoie le produit maximal de `k` facteurs positifs consécutifs dans une liste de `valeurs`.

> On garantit que `valeurs` est de taille au moins égale à `k` et que `k > 0`.

!!! example "Example"
    ```pycon
    >>> produit_maxi([0, 1, 2, 3, 2, 1, 0], 3)
    12
    >>> produit_maxi([0, 1, 2, 3, 2, 1, 0], 1)
    3
    ```

{{ IDE('exo', MAX=1000) }}


??? tip "Indice 1"
    On pourra commencer par résoudre avant le problème `k_somme`.

    On pourra commencer par faire le produit des valeurs **non nulles** parmi les `k` premières, ainsi que le nombre de zéros `nb_zeros`, pour initialiser une variable `maxi`.

    On pourra ensuite faire une boucle sur les valeurs restantes qui met à jour ces deux variables.

??? tip "Indice 2"
    On pourra s'aider du code à trou

    ```python
    def produit_maxi(valeurs, k):
        nb_zeros = ...
        produit = ...
        for i in range(k):
            x = valeurs[i]
            if x == 0:
                nb_zeros = ...
            else:
                produit = ...
        if nb_zeros == 0:
            maxi = ...
        else:
            maxi = ...
        
        for i in range(..., ...):
            x = valeurs[i]
            if x == 0:
                nb_zeros = ...
            else:
                produit = ...
            
            x = valeurs[i - k]
            if x == 0:
                nb_zeros = ...
            else:
                produit = ...
            
            if nb_zeros == ... and produit > maxi:
                maxi = ...
        
        return maxi
    ```
