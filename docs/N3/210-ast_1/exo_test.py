# Tests
assert analyse("35.896 ") == ["35.896"]
assert analyse("3*5   +8") == ["3", "*", "5", "+", "8"]
assert analyse("3.9  * (5+8.6)") == ["3.9", "*", "(", "5", "+", "8.6", ")"]

# Tests supplémentaires
assert analyse("") == []
assert analyse("1.986") == ["1.986"]
assert analyse("((3))") == list("((3))")
from random import choice, random, randrange

for num_test in range(20):
    jetons = []
    for _ in range(randrange(10, 100)):
        jetons.extend(
            [
                "(",
                str(randrange(0, 2000)),
                choice("+-*/"),
                " " * randrange(1, 3),
                str(100 * random()),
                ")",
                choice("+-*/"),
            ]
        )
    assert analyse("".join(jetons)) == [j for j in jetons if j[0] != " "]
