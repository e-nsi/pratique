def somme_minimale_1(grille):
    n = len(grille)
    sommes = []
    cumul = 0
    for x in grille[0]:
        cumul += x
        sommes.append(cumul)
    for i in range(1, n):
        sommes[0] = sommes[0] + grille[i][0]
        for j in range(1, n):
            sommes[j] = grille[i][j] + min(sommes[j], sommes[j - 1])
    return sommes[-1]


# tests

grille = [[1, 5, 9], [10, 3, 5], [10, 2, 3]]
assert somme_minimale_1(grille) == 14


grille = [
    [131, 673, 234, 103, 18],
    [201, 96, 342, 965, 150],
    [630, 803, 746, 422, 111],
    [537, 699, 497, 121, 956],
    [805, 732, 524, 37, 331],
]
assert somme_minimale_1(grille) == 2427
