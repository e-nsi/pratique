
# tests

parents = [None, 0, 0, 0, 2, 2, 2, 5, 5, 6, 6, 9, 9]
assert plus_petit_ancetre_commun(parents, 8, 11) == 2
assert plus_petit_ancetre_commun(parents, 1, 10) == 0
assert plus_petit_ancetre_commun(parents, 12, 6) == 6


# autres tests


parents = [None] + list(range(1000))
for a in range(10):
    attendu = a
    resultat = plus_petit_ancetre_commun(parents, a, 999 - a)
    assert resultat == attendu, "Erreur avec un arbre peigne"

parents = [None] + [0]*500 + [1]*500
for a in range(10):
    attendu = 0
    resultat = plus_petit_ancetre_commun(parents, 400 + a, a + 430)
    assert resultat == attendu
    attendu = 0
    resultat = plus_petit_ancetre_commun(parents, 400 + a, a + 530)
    assert resultat == attendu
    attendu = 1
    resultat = plus_petit_ancetre_commun(parents, 510 + a, a + 530)
    assert resultat == attendu
