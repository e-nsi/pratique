def supprime_premier(chaine, cible):
    ...


def anagrammes(chaine1, chaine2):
    ...


# Tests
assert supprime_premier('ukulélé', 'u') == (True, 'kulélé')
assert supprime_premier('ukulélé', 'é') == (True, 'ukullé')
assert supprime_premier('ukulélé', 'a') == (False, 'ukulélé')
assert anagrammes('chien', 'niche')
assert anagrammes('énergie noire', 'reine ignorée')
assert not anagrammes('louve', 'poule')
