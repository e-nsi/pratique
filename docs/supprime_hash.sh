#!/bin/sh

nom_fichier_secret="clef.txt"


supprime_hash () {
  dossier=$1
  fichier_clef="$dossier/$nom_fichier_secret"
  if [ -d "$dossier" ]
  then
      fichier_clef="$dossier/$nom_fichier_secret"
      #echo "le fichier secret est $fichier_clef"      
      if [ -f "$fichier_clef" ]
      then
          # on a une clef, sinon on n'a rien à faire
	      hash=$(echo -e "e-nsi+$(cat ${fichier_clef})" | md5sum | sed s/\ .*//)
          if [ -d "$dossier/$hash" ]
          then
              # il y a bien le dossier de hash              
              for fich in "$dossier/$hash/"*
              do
                  git mv $fich "$dossier/"
              done
              #echo git mv "$dossier/$hash/"* "$dossier"
              git clean -d -f "$dossier/$hash"
          fi
          git rm "$fichier_clef"
      fi
  fi
}


if [ $# = 0 ]
then
    echo "Déplace les fichiers de correction et de remarques depuis le fichier de hash vers la racine de l'exo."
    echo "Supprime le dossier de hash et le fichier de clef."
    echo "Usage : verif_niveau.sh DOSSIER [DOSSIER]*"
    echo "DOSSIER est un dossier d'exercice (N1/mon_exo)"
    exit 1
else
    while [ $# -gt 0 ]
    do
        supprime_hash "$1"
	shift
    done
fi


