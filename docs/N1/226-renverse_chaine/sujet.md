---
author: Nicolas Revéret
title: Renverser une chaine
tags:
  - 2-string
---

# Renverser une chaine

Programmer une fonction `renverser` :

* prenant en paramètre une chaine de caractères `mot`,
  
* renvoyant une nouvelle chaine de caractères en inversant ceux de la chaine `mot`.

!!! warning "Contrainte"

    On s'interdit d'utiliser les fonctions `reverse` et `reversed`, ainsi que les tranches `mot[::-1]`.


```pycon
>>> renverser('informatique')
'euqitamrofni'
>>> renverser('nsi')
'isn'
>>> renverser('')
''
```


{{ IDE('exo', SANS="reverse, reversed") }}
