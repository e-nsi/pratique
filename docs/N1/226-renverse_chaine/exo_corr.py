def renverser(mot):
    tom = ''
    for caractere in mot:
        tom = caractere + tom
    return tom


# Tests
assert renverser('informatique') == 'euqitamrofni'
assert renverser('nsi') == 'isn'
assert renverser('') == ''

