def correspond(mot_complet, mot_a_trous):
    ...





# tests

assert correspond("INFORMATIQUE", "INFO.MA.IQUE") == True
assert correspond("AUTOMATIQUE", "INFO.MA.IQUE") == False
assert correspond("INFO", "INFO.MA.IQUE") == False
assert correspond("INFORMATIQUES", "INFO.MA.IQUE") == False

