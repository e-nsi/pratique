def scores_aimes(votes):
    """
    Renvoie le dictionnaire dont les clés sont les personnes,
    et les valeurs le nombre de personnes qui leur ont attribué des J'aime.
    """
    ...


def gagnants(votes):
    """
    Renvoie la liste des personnes ayant le plus reçu de : J'aime
    """
    ...



# Tests
votes_soiree = {
    "Alice": ["Bob", "Carole", "Dylan"],
    "Bob": ["Carole", "Esma"],
    "Esma": ["Bob", "Alice"],
    "Fabien": ["Dylan"],
    "Carole": [],
    "Dylan":[],
}


assert scores_aimes(votes_soiree) == {
    "Bob": 2,
    "Alice": 1,
    "Esma": 1,
    "Fabien": 0,
    "Carole": 2,
    "Dylan": 2,
}

assert sorted(gagnants(votes_soiree)) == ["Bob", "Carole", "Dylan"]

