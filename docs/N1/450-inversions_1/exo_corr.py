def inversions(tableau):
    total = 0
    taille = len(tableau)
    for i in range(taille - 1):
        for j in range(i + 1, taille):
            if tableau[i] > tableau[j]:
                total += 1

    return total


# Tests
assert inversions([]) == 0
assert inversions([5, 6, 7, 9]) == 0
assert inversions([7, 5, 9, 6]) == 3