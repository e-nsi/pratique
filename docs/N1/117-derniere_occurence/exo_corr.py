def derniere_occurrence(tableau, cible):
    indice_dernier = len(tableau)
    for i in range(len(tableau)):
        if tableau[i] == cible:
            indice_dernier = i
    return indice_dernier


# tests

assert derniere_occurrence([5, 3], 1) == 2
assert derniere_occurrence([2, 4], 2) == 0
assert derniere_occurrence([2, 3, 5, 2, 4], 2) == 3
