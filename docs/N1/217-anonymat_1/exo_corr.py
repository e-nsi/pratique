def noircir(texte, noir):
    resultat = ""
    for caractere in texte:
        if caractere.isalpha():
            resultat += noir
        else:
            resultat += caractere

    return resultat


# Tests
assert noircir("", "*") == ""
assert noircir("L'espion était J. Bond", "▮") == "▮'▮▮▮▮▮▮ ▮▮▮▮▮ ▮. ▮▮▮▮"
assert noircir("L'espion était J. Bond", "_") == "_'______ _____ _. ____"
assert noircir(";-)", "▮") == ";-)"
