def noircir(texte, noir):
    ...


# Tests
assert noircir("", "*") == ""
assert noircir("L'espion était J. Bond", "▮") == "▮'▮▮▮▮▮▮ ▮▮▮▮▮ ▮. ▮▮▮▮"
assert noircir("L'espion était J. Bond", "_") == "_'______ _____ _. ____"
assert noircir(";-)", "▮") == ";-)"
assert noircir("*", "*") == "*"
