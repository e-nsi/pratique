def position_minimum(tableau, i):
    "recherche et renvoie l'indice du minimum à partir de l'indice i"
    ind_mini = i
    for j in range(i + 1, len(tableau)):
        if tableau[j] < tableau[ind_mini]:
            ind_mini = j
    return ind_mini

def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]

def tri_selection(tableau):
    for i in range(len(tableau)):
        indice_minimum = position_minimum(tableau, i)
        echange(tableau, indice_minimum, i)


# tests

tab = [1, 52, 6, -9, 12]
tri_selection(tab)
assert tab == [-9, 1, 6, 12, 52], "Exemple 1"

tab_vide = []
tri_selection(tab_vide)
assert tab_vide == [], "Exemple 2"

singleton = [9]
tri_selection(singleton)
assert singleton == [9], "Exemple 3"
