def correspond(adresse, reference):
    longueur_adr = len(adresse)
    longueur_ref = len(reference)
    
    i_adr = 0
    i_ref = 0
    
    local = True
    
    while (i_adr < longueur_adr) and (i_ref < longueur_ref):
        if adresse[i_adr] == "." and local:
            i_adr += 1
        elif adresse[i_adr] != reference[i_ref]:
            return False
        else:
            if reference[i_ref] == "@":
                local = False
            i_adr += 1
            i_ref += 1
    return (i_adr == longueur_adr) and (i_ref == longueur_ref)


# Tests
assert correspond("courspremiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours.premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("co.ur.spremi.ere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours..premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond(".courspremiere.@e-nsi.fr", "courspremiere@e-nsi.fr") == True
assert correspond("cours_premiere@e-nsi.fr", "courspremiere@e-nsi.fr") == False
assert correspond("courspremier@e-nsi.fr", "courspremiere@e-nsi.fr") == False
assert correspond("courspremiere@aeif.org", "courspremiere@e-nsi.fr") == False
