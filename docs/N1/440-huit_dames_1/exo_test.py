# Tests
valide = [
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0],
]

invalide = [[0, 0, 1], [1, 0, 0], [0, 1, 0]]

assert donne_ligne(valide, 0) == [0, 0, 0, 1, 0, 0, 0, 0]
assert donne_colonne(valide, 2) == [0, 0, 1, 0, 0, 0, 0, 0]
assert donne_diagonale_NE(valide, 4, 0) == [0, 0, 1, 0, 0]
assert donne_diagonale_NO(valide, 1, 1) == [0, 0]
assert donne_diagonale_SE(valide, 0, 0) == [0, 0, 1, 0, 0, 0, 0, 0]
assert donne_diagonale_SO(valide, 3, 7) == [1, 0, 0, 0, 0]
assert disposition_valide(valide)
assert not disposition_valide(invalide)

# Tests supplémentaires
valide_2 = [
    [0, 0, 0, 0, 1, 0, 0, 0],
    [0, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, 0],
    [0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 1, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0],
]

valide_3 = [[1]]
invalide_2 = [[1, 0], [1, 1]]

for i, ligne in enumerate(valide_2):
    assert donne_ligne(valide_2, i) == ligne
for j in range(len(valide_2)):
    assert donne_colonne(valide_2, j) == [ligne[j] for ligne in valide_2]
assert donne_diagonale_SE(valide_2, 0, 0) == [0, 1, 0, 0, 0, 0, 0, 0]
assert donne_diagonale_SO(valide_2, 1, 7) == [0, 0, 0, 0, 0, 0, 0]
assert donne_diagonale_NE(valide_2, 7, 3) == [0, 0, 0, 0, 0]
assert donne_diagonale_NO(valide_2, 7, 7) == [0, 0, 0, 0, 0, 0, 1, 0]
assert donne_diagonale_SE(invalide_2, 0, 0) == [1, 1]
assert donne_diagonale_SO(invalide_2, 1, 0) == [1]
assert donne_diagonale_NE(invalide_2, 0, 1) == [0]
assert donne_diagonale_NO(invalide_2, 0, 0) == [1]
assert disposition_valide(valide_2)
assert disposition_valide(valide_3)
assert not disposition_valide(invalide_2)

invalide_3 = [[0, 0, 0, 0], [0, 0, 0, 0], [1, 0, 1, 0], [0, 0, 0, 0]]
invalide_4 = [[0, 1, 0, 0], [0, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0]]
invalide_5 = [[0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 1, 0]]
invalide_6 = [[0, 0, 1, 0], [0, 0, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0]]

assert not disposition_valide(invalide_3)
assert not disposition_valide(invalide_4)
assert not disposition_valide(invalide_5)
assert not disposition_valide(invalide_6)
