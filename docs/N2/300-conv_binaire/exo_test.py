# tests

assert conversion_binaire(13) == [1, 1, 0, 1]
assert conversion_binaire(4) == [1, 0, 0]
assert conversion_binaire(0) == [0]


# autres tests

for n in range(100):
    attendu = list(map(int, bin(n)[2:]))
    assert conversion_binaire(n) == attendu, f"Erreur avec {n}"
