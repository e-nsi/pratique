# Tests
train = Train()
w1 = Wagon("blé")
train.ajoute_wagon(w1)
w2 = Wagon("riz")
train.ajoute_wagon(w2)
train.ajoute_wagon(Wagon("sable"))
assert str(train) == 'Locomotive - Wagon de blé - Wagon de riz - Wagon de sable'
assert not train.est_vide()
assert train.donne_nb_wagons() == 3
assert train.transporte_du('blé')
assert not train.transporte_du('matériel')
assert train.supprime_wagon_de('riz')
assert str(train) == 'Locomotive - Wagon de blé - Wagon de sable'
assert not train.supprime_wagon_de('riz')


# Tests secrets
train = Train()
for k in range(10):
    train.ajoute_wagon(Wagon('A'))
    train.ajoute_wagon(Wagon('B'))
    assert train.donne_nb_wagons() == 2 * (k + 1)
    assert train.nb_wagons == train.donne_nb_wagons()
assert str(train) == 'Locomotive - ' + \
    ' - '.join(['Wagon de A', 'Wagon de B'] * 10)
assert train.transporte_du('A')
assert train.transporte_du('B')
assert not train.transporte_du('C')
for k in range(10):
    assert train.supprime_wagon_de('A')
    assert train.donne_nb_wagons() == 20 - (k + 1)
assert str(train) == 'Locomotive - ' + ' - '.join(['Wagon de B'] * 10)
assert not train.supprime_wagon_de('A')
assert not train.supprime_wagon_de('C')
