# tests
assert correspond("AA", "AAGGTTCC", 4) == False
assert correspond("AT", "ATGCATGC", 4) == True

assert est_inclus("AATC", "GTACAAATCTTGCC") == True
assert est_inclus("AGTC", "GTACAAATCTTGCC") == False
assert est_inclus("AGTC", "GTACAAATCTTGCA") == False
assert est_inclus("AGTC", "GTACAAATCTAGTC") == True



# autres tests
assert correspond("AA", "AAGGTTCC", 7) == False
assert correspond("CC", "AAGGTTCC", 6) == True
assert est_inclus("AAAA", "AAAAGTATCTTGCC") == True
assert est_inclus("GTAC", "GTACAAATCTTGCC") == True
assert est_inclus("AAAA", "GTACAAATCTAAAA") == True
assert est_inclus("AGTA", "GTACAAATCTAGTA") == True
assert est_inclus("AGTA", "GTACAAATCTAGTC") == False
