# Tests publics
medor = Chien("Médor", 12.0)
assert medor.donne_nom() == "Médor"
assert medor.donne_poids() == 12.0
assert medor.machouille("bâton") == "bâto"
assert medor.aboie(3) == "OuafOuafOuaf"
assert medor.mange(2.0) == False
assert medor.mange(1.0) == True
assert medor.donne_poids() == 13.0
assert medor.mange(1.3) == True

# Tests secrets
toutou = Chien("toutou", 10.0)
assert toutou.donne_nom() == "toutou", "Erreur sur la méthode donne_nom"
toutou.nom = "Toutou"
assert toutou.donne_nom() == "Toutou", "Attribut mal nommé"
assert toutou.donne_poids() == 10.0, "Erreur sur la méthode donne_poids"
print("Tests du constructeur passés avec succès")
toutou.poids = 11.0
assert toutou.donne_poids() == 11.0, "Attribut mal nommé"
assert toutou.machouille("balle") == "ball", "Erreur sur la méthode machouille"
assert toutou.machouille("") == "", "Erreur sur la méthode machouille"
print("Tests de la méthode 'machouille' passés avec succès")
assert toutou.aboie(30) == "Ouaf" * 30, "Erreur sur la méthode aboie"
assert toutou.aboie(0) == "", "Erreur sur la méthode aboie"
print("Tests de la méthode 'aboie' passés avec succès")
assert not toutou.mange(-1.0), "Erreur sur la méthode mange"
assert not toutou.mange(11.0), "Erreur sur la méthode mange"
assert not toutou.mange(0.0), "Erreur sur la méthode mange"
assert toutou.mange(1.1), "Erreur sur la méthode mange"
assert toutou.donne_poids() == 12.1, "Erreur sur la méthode mange"
print("Tests de la méthode 'mange' passés avec succès")
