# 🗒️ Présentation

Vous trouverez ici des exercices pour lesquels l'algorithme est plus élaboré.

Les exercices sont guidés : le code est partiellement saisi et il faut le compléter... 


![](./John_H_Conway_2005.jpg)

> [John Horton Conway](https://fr.wikipedia.org/wiki/John_Horton_Conway), né le 26 décembre 1937 à Liverpool et mort le 11 avril 2020 à New Brunswick (New Jersey), est un mathématicien britannique. Il s'est intéressé aux théories des groupes finis, des nœuds, des nombres, des jeux et du codage.
> [...]
> Son intérêt et sa curiosité pour les mathématiques le poussent aussi dans des registres plus ludiques concernant les énigmes et la vulgarisation en direction des jeunes. Il consacre ainsi durant plusieurs années, deux semaines de son été à animer des camps mathématiques destinés à des jeunes de 15 à 18 ans et de 11 à 14 ans comme le Canada/USA Mathcampath et le mathPath. Pour lui, les jeux et les énigmes mathématiques sont un moyen d'enseigner les concepts mathématiques. À Princeton, il suscite la curiosité de ses étudiants par des jeux et énigmes à base de dés, de cordes et de cartes et son bureau est rempli de modèles mathématiques en bois ou papier illustrant des concepts mathématiques.
