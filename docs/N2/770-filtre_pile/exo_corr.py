class Pile:
    def __init__(self, valeurs=[]):
        self.valeurs = list(valeurs)
    
    def est_vide(self):
        return self.valeurs == list()
    
    def __str__(self):
        "Affiche la pile, en indiquant le sommet"
        return "| " + " | ".join(map(str, self.valeurs)) + ' <- sommet'
    
    def depile(self):
        if self.est_vide():
            raise ValueError("Erreur, pile vide")
        else:
            return self.valeurs.pop()

    def empile(self, element):
        self.valeurs.append(element)
    

def filtre_positifs(donnees):
    auxiliaire = Pile()
    while not donnees.est_vide():
        element = donnees.depile()
        auxiliaire.empile(element)
    
    resultat = Pile()
    while not auxiliaire.est_vide():
        element = auxiliaire.depile()
        donnees.empile(element)
        if element >= 0:
            resultat.empile(element)
    
    return resultat
