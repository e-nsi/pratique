# tests

donnees = Pile([4, -11, 7, -3, -1, 0, 6])
assert str(filtre_positifs(donnees)) == '| 4 | 7 | 0 | 6 <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| 4 | -11 | 7 | -3 | -1 | 0 | 6 <- sommet'

donnees = Pile([1, 2, 3, 4])
assert str(filtre_positifs(donnees)) == '| 1 | 2 | 3 | 4 <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| 1 | 2 | 3 | 4 <- sommet'

donnees = Pile([-4, -3, -2, -1])
assert str(filtre_positifs(donnees)) == '|  <- sommet'
assert donnees.est_vide() == False
assert str(donnees) == '| -4 | -3 | -2 | -1 <- sommet'

# autres tests

donnees = Pile([])
assert str(filtre_positifs(donnees)) == '|  <- sommet', "Erreur avec donnees = []"
assert donnees.est_vide() == True
assert str(donnees) == '|  <- sommet'

donnees = Pile([6])
assert str(filtre_positifs(donnees)) == '| 6 <- sommet', "Erreur avec donnees = [6]"
assert donnees.est_vide() == False
assert str(donnees) == '| 6 <- sommet'

donnees = Pile([-6])
assert str(filtre_positifs(donnees)) == '|  <- sommet', "Erreur avec donnees = [-6]"
assert donnees.est_vide() == False
assert str(donnees) == '| -6 <- sommet'

donnees = Pile([-7, 6, -5, 4, -3, 2, -1])
assert str(filtre_positifs(donnees)) == '| 6 | 4 | 2 <- sommet', "Erreur avec donnees = [-7, 6, -5, 4, -3, 2, -1]"
assert donnees.est_vide() == False
assert str(donnees) == '| -7 | 6 | -5 | 4 | -3 | 2 | -1 <- sommet'

