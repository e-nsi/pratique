def eratosthene(n):
    crible = [True] * (n + 1)
    crible[0] = False
    if n > 0:
        crible[1] = False
    for p in range(n + 1):
        if crible[p] == True:
            # p est premier
            for kp in range(2*p, n + 1, p):
                crible[kp] = False
    return crible

def premiers_jusque(n):
    crible = eratosthene(n)
    premiers = [p for p in range(n + 1) if crible[p]]
    return premiers
